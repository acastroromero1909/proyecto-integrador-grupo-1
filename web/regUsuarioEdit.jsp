<%@page import="modeloDAO.usuarioDAO"%>
<%@page import="modelo.Usuario"%>
<!DOCTYPE html>
<!-- Created by CodingLab |www.youtube.com/c/CodingLabYT-->
<html lang="en" dir="ltr">
  <head>
    <title>GJ-Store</title>
    <link rel="icon" type="image" href="Images/logoSolo.png" >
    <meta charset="UTF-8">
    <!--<title> Login and Registration Form in HTML & CSS | CodingLab </title>-->
    <link rel="stylesheet" href="css/styles.css">
    <!-- Fontawesome CDN Link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="css/inicio.css" rel="stylesheet" type="text/css"/>
         <link href="css/formularios.css" rel="stylesheet" type="text/css"/>
   </head>
   <header>
        <%@include file="recursos/Navbar.jsp" %>
    </header>
<body>
    <%
         HttpSession sesion = request.getSession();
         usuarioDAO  obj=new usuarioDAO();
         Usuario u=(Usuario)request.getAttribute("dato");
         int id=(Integer)request.getAttribute("id");
     %>
  <div class="container">
    <input type="checkbox" id="flip">
    <div class="cover">
      <div class="front">
        <img src="Images/frontImg.jpg" alt="">
        
      </div>
      <div class="back">
        <img class="backImg" src="Images/backImg.jpg" alt="">
        
      </div>
    </div>
    <div class="forms">
        <div class="form-content">
          
        <div class="signup-form">
          <div class="title">Actualizar informaci�n</div>
        <form action="control" id="id_form" enctype="multipart/form-data">
            <input type="hidden" name="opc" value="10">
            <input name="codu" type="hidden" value="<%=u.getIdUser()%>">
            <div class="input-boxes">
              <div class="input-box">
                <i class="fas fa-user"></i>
                <input class="form-control" type="text" id="id_nombre" name="nombre" value="<%=u.getNomUser()%>" placeholder="Ingrese el nombre"  pattern="[A-Za-z/��]+" required title="Solo ingrese Letras">
              </div>
              <div class="input-box">
                <i class="fas fa-user"></i>
                <input class="form-control" type="text" id="id_apellido" name="apellido" value="<%=u.getApeUser()%>"placeholder="Ingrese el apellido"  pattern="[A-Za-z/��]+" required title="Solo ingrese Letras">
              </div>
              <div class="input-box">
                <i class="fas fa-envelope"></i>
                <input class="form-control" type="email" id="id_correo" name="correo" value="<%=u.getCorrUser()%>" placeholder="Ingrese el correo" required>
              </div>
              <div class="input-box">
                <i class="fas fa-lock"></i>
                <input class="form-control" type="password" id="id_contrasena" name="contrasena" value="<%=u.getPassUser()%>" placeholder="Ingrese el contrasena" required>
              </div>
              <div class="input-box">
                <i class="fas fa-user"></i>
                <input class="form-control" type="text" id="id_correo" name="celular"value="<%=u.getCelUser()%>" placeholder="Ingrese el celular" minlength="9" maxlength="9" required pattern="[0-9]+" title="Solo ingrese n�meros">              </div>
              <div class="input-box">
                <i class="fas fa-user"></i>
                <input class="form-control" type="text" id="id_direccion" name="direccion" value="<%=u.getDirUser()%>" placeholder="Ingrese el direccion" required>
              </div>
              <div class="button input-box">
                <input type="submit" value="Guardar datos"onclick="validacion()">
              </div>
              <div class="text sign-up-text"><a href="index.jsp">Cancelar proceso</a></div>
            </div>
      </form>
              
    </div>
    </div>
    </div>
  </div>
              <script>
               function validacion(){
                   
                    alert('Datos actualizados');  
                 
                }
            </script> 
</body>
</html>
