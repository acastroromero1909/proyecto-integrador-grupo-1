<%@page import="modeloDAO.productoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="modelo.*, java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>GJ-Store</title>
        <link rel="icon" type="image" href="Images/logoSolo.png" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link href="css/inicio.css" rel="stylesheet" type="text/css"/>
        <script src="https://kit.fontawesome.com/d217ba9243.js" crossorigin="anonymous"></script>

        
    </head>
    <header>
        <%@include file="recursos/Navbar.jsp" %>
    </header>
    <body  id="cuerpo">

        <%

            List<Producto> lis = (ArrayList) request.getAttribute("dato");
            Producto p = new Producto();
            int pi = lis.get(0).getId();
            HttpSession sesion = request.getSession();
            String correo = (String) sesion.getAttribute("correo");
            String nombre = (String) sesion.getAttribute("nom");
            Integer idU = (Integer) sesion.getAttribute("id");

        %>

    <center>
        <div class="card mb-3" id="cardP" style="width: 90%">
        <form name="fr" action="control">
            <table class="table table-hover " style="width: 90%" >
                <%-- <tr><th colspan="3" id="titulo">Detalle del Producto--%>
                <input type="hidden" name="opc" value="27">
                <input type="hidden" name="coda" value="<%=p.getId()%>">
                <tr style="background-color:white"><td rowspan="6" width="50%" style="background-color:'white'"><img src="<%=lis.get(0).getLinkProIMG()%>" width="70%" height="70%">
                <tr><th colspan="2"><h5><%=lis.get(0).getNombre()%></h5>
                <tr><th>Modelo<td><%=lis.get(0).getModelo()%></tr>
                <tr><th>Marca<td><%=lis.get(0).getMarca()%></tr>
                <tr><th>Precio Unitario<td>S/.<%=lis.get(0).getPrecio()%>0</tr>
                <tr style="background-color:white"><td>
                        <% if (correo == null && nombre == null) {%>
                        <a class="btn btn-outline-danger" href="control?opc=0&id=<%=pi%>"> + Lista de deseos</a>
                        <%} else {%>
                        <a class="btn btn-outline-danger" href="control?opc=agregarDeseo&id=<%=pi%>&idU=<%=idU%>"> + Lista de deseos</a>
                        <%}%> 
                    </td>


                </tr>

            </table>  
        </form>
        </div>           
        <div class="card mb-3" id="cardP" style="width: 90%">
        <table class="table table-striped" >
            <tr><th>Codigo<th>Nombre<th>Precio<th>Tienda<Th>Ver
                    <%
                        for (Producto x : lis) {
                            out.print("<tr><td>" + x.getId() + "<td>" + x.getNombre() + "<td> S/." + x.getPrecio() + "0<td><img src='" + x.getTiendaIMG() + "' alt='No hay' width=80 height=80 class='rounded-circle'>" + "<td><a href='" + x.getLinkTienda() + "'><i class='fa-solid fa-eye fa-2x'></i></a>");
                    %>


                    <%
                        }
                    %>
        </table>
        </div>


    </center>

</body>
</html>