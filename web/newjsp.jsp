<%-- 
    Document   : newjsp
    Created on : 24/11/2022, 10:14:02 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página card</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/dark.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">

    <script src="https://kit.fontawesome.com/03a89292db.js" crossorigin="anonymous"></script>
</head>

<body class="">

    <h1>Nuestro equipo de expertos</h1>
    <p class="descripcion">Lorem Ipsum es simplemente texto de relleno de la industria de la impresión y la composición
        tipográfica</p>

    <div class="modo" id="modo">
        <i class="fas fa-toggle-on"></i>
    </div>

    

    <script src="js/main.js"></script>
</body>

</html>
