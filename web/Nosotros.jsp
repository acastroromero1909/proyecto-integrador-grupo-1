<%-- 
    Document   : Nosotros
    Created on : 18/11/2022, 08:44:52 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
   <title>GJ-Store</title>
    <link rel="icon" type="image" href="Images/logoSolo.png" >
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
   <link href="css/inicio.css" rel="stylesheet" type="text/css"/>
   <%@include file="recursos/Navbar.jsp" %>
 </head>
 <header>
        <%@include file="recursos/Navbar.jsp" %>
    </header>
 <body id="cuerpo" style="background-color: #16191C;">
 <div class="container" >
   <div id="accordion" >
     <div class="card"> 
         <a class="card-link" data-toggle="collapse" href="#collapseOne">
            <div class="card-header">
                <h4>¿Cómo agrego un producto a mi lista de favoritos?</h4>
            </div>
         </a>      
       <div id="collapseOne" class="collapse show" data-parent="#accordion">
         <div class="card-body">
             Para agregar un producto a tu wishlist necesitas estar logueado en Compy, luego solo necesitas dar click en el botón "Agregar a favoritos" en la página del producto. Adicionalmente, también puedes agregar un producto desde la galería dando click al ❤️‍.
         </div>
       </div> 
     </div>
       <div class="card"> 
         <a class="card-link" data-toggle="collapse" href="#collapseTwo">
            <div class="card-header">
                <h4>  ¿El precio mostrado no coincide con el de la tienda?</h4>
            </div>
         </a>     
       <div id="collapseTwo" class="collapse show" data-parent="#accordion">
         <div class="card-body">
             Existe la pequeña posibilidad de que un precio no coincida. Esto se debe principalmente a que los precios dentro de compy no se encuentran en tiempo real y solo se actualizan 2 veces al día.
         </div>
       </div> 
     </div>
       <div class="card"> 
         <a class="card-link" data-toggle="collapse" href="#collapseThree">
            <div class="card-header">
                <h4>   ¿Cómo reporto si un producto está mal agrupado o presenta algún error?</h4>
            </div>
         </a>
       
       <div id="collapseThree" class="collapse show" data-parent="#accordion">
         <div class="card-body">
            Para reportar un producto solo es necesario enviarnos un mensaje a nuestras redes con el link del producto y nosotros con gusto lo resolveremos.
         </div>
       </div> 
     </div>
       <div class="card"> 
         <a class="card-link" data-toggle="collapse" href="#collapseFour">
            <div class="card-header">
                <h4>  Políticas de seguridad</h4>
            </div>
         </a>
       
       <div id="collapseFour" class="collapse show" data-parent="#accordion">
         <div class="card-body">
                GJ-STORE adoptará las medidas necesarias y prudentes para resguardar la seguridad de tus datos y de tu clave secreta. En caso de detectarse cambios en la información que has registrado en el Sitio, o bien, ante cualquier irregularidad en las transacciones relacionadas con tu identificación o la del medio de pago, o simplemente como medida de protección a tu identidad, nuestros ejecutivos podrán contactarte por vía telefónica o correo electrónico, a fin de corroborar tus datos e intentar evitar posibles fraudes.         </div>
       </div> 
     </div>

   </div>
 </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
 </body>
 </html> 
