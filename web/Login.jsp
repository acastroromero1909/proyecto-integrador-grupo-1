<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>GJ-Store</title>
    <link rel="icon" type="image" href="Images/logoSolo.png" >
    <meta charset="UTF-8">
    
    <link rel="stylesheet" href="css/styles.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="css/inicio.css" rel="stylesheet" type="text/css"/>
         <link href="css/formularios.css" rel="stylesheet" type="text/css"/>
   </head>
   <header>
        <%@include file="recursos/Navbar.jsp" %>
    </header>
<body style="background-color: #16191C;">
    <br>
    <br>
    <br>
    <br>
  <div class="container">
    <input type="checkbox" id="flip">
    <div class="cover">
      <div class="front">
        <img src="Images/frontImg2.jpeg" alt="">
        
      </div>
      <div class="back">
        <img class="backImg" src="Images/backImg.jpg" alt="">
        
      </div>
    </div>
    <div class="forms">
        <div class="form-content">
          <div class="login-form">
            <div ><img src="https://compy.pe/assets/images/icon/logofinal.png" height="40" width="120"></div>  
            <br>
            <div class="title">Iniciar Sesi�n</div>
          <form action="control">
            <input type="hidden" name="opc" value="0">
            <div class="input-boxes">
              <div class="input-box">
                <i class="fas fa-envelope"></i>
                <input class="form-control" type="text" id="id_correo" name="correo" placeholder="Ingrese el correo" required>
              </div>
              <div class="input-box">
                <i class="fas fa-lock"></i>
                <input type="password" id="id_contrasena" name="contrasena" placeholder="Ingrese el contrasena" required>
              </div>
              <!--<div class="text"><a href="#">Forgot password?</a></div>-->
              <div class="button input-box" style="color: #F98544">
                 
                  <input  type="submit" name="accion" value="  Iniciar sesi�n    ">
                
              </div>
              <div class="text sign-up-text">�No tienes una cuenta? <label for="flip">Reg�strese ahora</label></div>
            </div>
        </form>
      </div>
        <div class="signup-form">
          <div class="title">Registrarse</div>
        <form action="control" id="id_form" enctype="multipart/form-data">
            <input type="hidden" name="opc" value="7">
            <div class="input-boxes">
              <div class="input-box">
                <i class="fas fa-user"></i>
                <input class="form-control" type="text" id="id_nombre" name="nombre" placeholder="Ingrese el nombre"  pattern="[A-Za-z/��]+" required title="Solo ingrese Letras">
              </div>
              <div class="input-box">
                <i class="fas fa-user"></i>
                <input class="form-control" type="text" id="id_apellido" name="apellido" placeholder="Ingrese el apellido"  pattern="[A-Za-z/��]+" required title="Solo ingrese Letras">
              </div>
              <div class="input-box">
                <i class="fas fa-envelope"></i>
                <input class="form-control" type="email" id="id_correo" name="correo" placeholder="Ingrese el correo" required>
              </div>
              <div class="input-box">
                <i class="fas fa-lock"></i>
                <input class="form-control" type="password" id="id_contrasena" name="contrasena" placeholder="Ingrese el contrasena" required>
              </div>
              <div class="input-box">
                <i class="fas fa-user"></i>
                <input class="form-control" type="text" id="id_correo" name="celular" placeholder="Ingrese el celular" minlength="9" maxlength="9" required pattern="[0-9]+" title="Solo ingrese n�meros">              </div>
              <div class="input-box">
                <i class="fas fa-user"></i>
                <input class="form-control" type="text" id="id_direccion" name="direccion" placeholder="Ingrese el direccion" required>
              </div>
              <div class="button input-box">
                <input type="submit" value="Registrar">
              </div>
              <div class="text sign-up-text">�Ya tienes una cuenta?  <label for="flip">Inicia sesi�n ahora</label></div>
            </div>
      </form>
    </div>
    </div>
    </div>
  </div>
</body>
</html>
