
<%@page import="modeloDAO.usuarioDAO"%>
<%@page import="modelo.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>GJ-Store</title>
        <link rel="icon" type="image" href="Images/logoSolo.png" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="css/inicio.css" rel="stylesheet" type="text/css"/>
        <link href="css/formularios.css" rel="stylesheet" type="text/css"/>
        
    </head>
    <header>
        <%@include file="recursos/Navbar.jsp" %>
    </header>
    <br>
    <br>
    <body>
    <%
         usuarioDAO  obj=new usuarioDAO();
     %>
        <div class="container">
        <h1>Registra Nuevo Usuario</h1>

        <form action="control" id="id_form" enctype="multipart/form-data"> 
                                <input type="hidden" name="opc" value="7">



                                <div class="form-group">
                                        <label class="control-label" for="id_nombre">Nombres</label>
                                        <input class="form-control" type="text" id="id_nombre" name="nombre" placeholder="Ingrese el nombre"  pattern="[A-Za-z/Ññ]+" required title="Solo ingrese Letras">
                                </div>
                                <div class="form-group">
                                        <label class="control-label" for="id_apellido">Apellido</label>
                                        <input class="form-control" type="text" id="id_apellido" name="apellido" placeholder="Ingrese el apellido"  pattern="[A-Za-z/Ññ]+" required title="Solo ingrese Letras">
                                </div>
                                <div class="form-group">
                                        <label class="control-label" for="id_correo">Correo</label>
                                        <input class="form-control" type="email" id="id_correo" name="correo" placeholder="Ingrese el correo" required>
                                </div>
                                <div class="form-group">
                                        <label class="control-label" for="id_contrasena">Contraseña</label>
                                        <input class="form-control" type="password" id="id_contrasena" name="contrasena" placeholder="Ingrese el contrasena" required>
                                </div>
                                <div class="form-group">
                                        <label class="control-label" for="id_celular">Celular</label>
                                        <input class="form-control" type="text" id="id_correo" name="celular" placeholder="Ingrese el celular" minlength="9" maxlength="9" required pattern="[0-9]+" title="Solo ingrese números">
                                </div>
                                <div class="form-group">
                                        <label class="control-label" for="id_direccion">Dirección</label>
                                        <input class="form-control" type="text" id="id_direccion" name="direccion" placeholder="Ingrese el direccion" required>
                                </div>



                                <div class="form-group">
                                            <button type="submit" class="btn btn-primary" >Guardar</button>
                                </div>
                </form>
        </div>

        </body>
        </html>
