<%@page import="modeloDAO.*"%>
<html>
    <head>
        <title>GJ-Store</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image" href="Images/logoSolo.png" >
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
        <link href="css/inicio.css" rel="stylesheet" type="text/css"/>
        <script src="https://kit.fontawesome.com/d217ba9243.js" crossorigin="anonymous"></script>
         <link href="css/estilo_car.css" rel="stylesheet" type="text/css"/>
         <link href="css/prueba2.css" rel="stylesheet" type="text/css"/>

    </head>
   
        <%@include file="recursos/Navbar.jsp" %>
         <body class="">
       <div class="iframe">
           <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="https://eloutput.com/app/uploads-eloutput.com/2019/11/Usabildiad-LG-TV-Nanocell.jpg" alt="First slide">
              <div class="carousel-caption d-none d-md-block">
                <h5>Los mejores productos en GJ-Store</h5>
                <p>Televisores</p>
                <div class="slider-btn">
                    <a href="control?opc=13&id=002"><button class="btn btn-1">Ver m�s productos</button></a>
                    <%--<button class="btn btn-2">Jennifer</button>--%>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://ideakreativa.net/wp-content/uploads/2020/04/laptops-econo%CC%81micas-para-disen%CC%83o.jpg" alt="Second slide">
              <div class="carousel-caption d-none d-md-block">
                <h5>Los mejores productos en GJ-Store</h5>
                <p>Laptops</p>
                <div class="slider-btn">
                    <a href="control?opc=13&id=001"><button class="btn btn-1">Ver m�s productos</button></a>
                    <%--<button class="btn btn-2">Jennifer</button>--%>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://cloudfront-us-east-1.images.arcpublishing.com/metroworldnews/6CJPKMIJVZBXVIJH77NPKZJZZY.jpg" alt="Third slide">
              <div class="carousel-caption d-none d-md-block">
                <h5>Los mejores productos en GJ-Store</h5>
                <p>Celulares</p>
                <div class="slider-btn">
                    <a href="control?opc=13&id=003"><button class="btn btn-1">Ver m�s productos</button></a>
                    <%--<button class="btn btn-2">Jennifer</button>--%>
                </div>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        </div>
        <div class="container2">
            <br>
            <div class="row w-100" id="card2">
                    <div class="col-md-4 col-sm-6">
                        <div class="single-works">
                            <img src="https://consumer.huawei.com/content/dam/huawei-cbg-site/latam/mx/mkt/plp/laptops/d14-amd-2021.jpg" alt="">
                            <div class="overlay-text">
                                <h3 class="title">LAPTOPS</h3>
                                <%--<span class="post">Posterior</span>--%>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="single-works">
                            <img src="https://hiraoka.com.pe/media/catalog/product/1/2/125718_ms32-k502.jpg" alt="">
                            <div class="overlay-text">
                                <h3 class="title">TELEVISORES</h3>
                                <%--<span class="post">Posterior</span>--%>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="single-works">
                            <img src="https://cdn.forbes.co/2020/11/Xiaomi-1280x720-JPG.jpg" alt="">
                            <div class="overlay-text">
                                <h3 class="title">CELULARES</h3>
                                <%--<span class="post">Posterior</span>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        <footer class="pie-pagina">
        <div class="grupo-1">
            <div class="box">
                <figure>
                    <a href="#">
                        <img src="Images/logo.png" alt="Logo ">
                    </a>
                </figure>
            </div>
            <div class="box">
                <h2>SOBRE NOSOTROS</h2>
                <p>Somos una empresa peruana que te permite comparar los precios de productos tecnol�gicos, mejorando tus compras.</p>
                
            </div>
            <div class="box">
                <h2>SIGUENOS</h2>
                <div class="red-social">
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-instagram"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-youtube"></a>
                </div>
            </div>
        </div>
        <div class="grupo-2">
            <small> Todos los Derechos Reservados.</small>
        </div>
    </footer>
    </center>
    
</body>
</html>