-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-10-2022 a las 15:04:41
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--
CREATE DATABASE IF NOT EXISTS `prueba` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `prueba`;

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `cambiaUser`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `cambiaUser` (IN `codu` INT, IN `nom` CHAR(255), IN `ape` CHAR(255), IN `corr` CHAR(255), IN `pass` CHAR(255), IN `cel` INT(9), IN `dir` CHAR(255))  update prueba.utp_usuario SET NO_USUA=nom ,NO_APEL=ape,DE_CORR=corr,DE_PASS=pass,NO_TELF=cel,DE_DIRE=dir
WHERE CO_USUA=codu$$

DROP PROCEDURE IF EXISTS `spAdiUser`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spAdiUser` (IN `nom` CHAR(255), IN `ape` CHAR(255), IN `corr` CHAR(255), IN `pass` CHAR(255), IN `cel` INT(9), IN `dir` CHAR(255))  BEGIN
declare nro int;
select IFNULL(max(CO_USUA),0)+1 into NRO from utp_usuario order by CO_USUA;
insert into utp_usuario values(nro,nom,ape,corr,pass,cel, dir);
    select nro;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `utp_categoria`
--

DROP TABLE IF EXISTS `utp_categoria`;
CREATE TABLE `utp_categoria` (
  `CO_CATE` varchar(3) NOT NULL,
  `DE_CATE` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELACIONES PARA LA TABLA `utp_categoria`:
--

--
-- Volcado de datos para la tabla `utp_categoria`
--

INSERT INTO `utp_categoria` (`CO_CATE`, `DE_CATE`) VALUES
('001', 'LAPTOP'),
('002', 'TELEVISOR'),
('003', 'CELULAR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `utp_lista_deseo`
--

DROP TABLE IF EXISTS `utp_lista_deseo`;
CREATE TABLE `utp_lista_deseo` (
  `CO_LIST` int(11) NOT NULL,
  `CO_PROD` int(11) NOT NULL,
  `CO_USUA` int(11) NOT NULL,
  `FE_REGI` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELACIONES PARA LA TABLA `utp_lista_deseo`:
--

--
-- Volcado de datos para la tabla `utp_lista_deseo`
--

INSERT INTO `utp_lista_deseo` (`CO_LIST`, `CO_PROD`, `CO_USUA`, `FE_REGI`) VALUES
(70, 18, 1, '26-10-2022'),
(71, 37, 1, '26-10-2022');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `utp_producto`
--

DROP TABLE IF EXISTS `utp_producto`;
CREATE TABLE `utp_producto` (
  `CO_PROD` int(11) NOT NULL,
  `DE_PROD` varchar(255) DEFAULT NULL,
  `IM_PREC` double DEFAULT NULL,
  `DE_MODE` varchar(255) DEFAULT NULL,
  `DE_MARC` varchar(255) DEFAULT NULL,
  `CO_CATE` varchar(3) NOT NULL,
  `DE_TIEN` varchar(255) DEFAULT NULL,
  `LK_TIEN` varchar(255) DEFAULT NULL,
  `LK_IMAG` varchar(255) DEFAULT NULL,
  `LK_LOGOTIEN` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELACIONES PARA LA TABLA `utp_producto`:
--

--
-- Volcado de datos para la tabla `utp_producto`
--

INSERT INTO `utp_producto` (`CO_PROD`, `DE_PROD`, `IM_PREC`, `DE_MODE`, `DE_MARC`, `CO_CATE`, `DE_TIEN`, `LK_TIEN`, `LK_IMAG`, `LK_LOGOTIEN`) VALUES
(1, 'LAPTOP HP 14CF2513LA INTEL CORE I3 10110U 4GB RAM 128GB SSD 14\'\' ', 1149, '14-cf2513la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-14cf2513la-intel-core-i3-10110u-4gb-ram-128gb-ssd-14-2004301204578p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004301204578/2004301204578_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(2, 'LAPTOP HP 14-DQ2510LA INTEL CORE I3 8GB RAM 512GB SSD 14\'\'', 1899, '14-dq2510la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-14-dq2510la-intel-core-i3-8gb-ram-512gb-ssd-14-2004293408701p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004293408701/2004293408701_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(3, 'LAPTOP LENOVO IDEAPAD 3I INTEL CORE I5 8GB RAM 512GB SSD 15.6\"', 2299, 'IDEAPAD 3i', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-3i-intel-core-i5-8gb-ram-512gb-ssd-156-2004298548051p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004298548051/2004298548051_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(4, 'LAPTOP GAMER ASUS TUF F15 FX506LHB-HN323UW INTEL CORE I5 10300H 8GB RAM 512GB SSD 15.6\'\'', 3199, 'FX506LHB-HN323W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-gamer-asus-tuf-f15-fx506lhb-hn323uw-intel-core-i5-10300h-8gb-ram-512gb-ssd-156-2004295827302p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004295827302/2004295827302_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(5, 'LAPTOP ASUS X515 X515JA-EJ3618W INTEL CORE I5 1035G1 8GB RAM 256GB SSD 15.6\'\'', 1999, 'X515JA-EJ3618W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-x515-x515ja-ej3618w-intel-core-i5-1035g1-8gb-ram-256gb-ssd-156-2004296008960p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004296008960/2004296008960_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(6, 'LAPTOP HP 15EF2505LA AMD RYZEN 7 5700U 8GB RAM 512GB SSD 15.6\'\'', 2799, '15-ef2505la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-15ef2505la-amd-ryzen-7-5700u-8gb-ram-512gb-ssd-156-2004284813286p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004284813286/2004284813286_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(7, 'LAPTOP LENOVO IDEAPAD 1 AMD RYZEN 5 8GB RAM 256GB SSD 14\"', 1899, 'IDEAPAD 1', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-1-amd-ryzen-5-8gb-ram-256gb-ssd-14-2004298832167p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004298832167/2004298832167_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(8, 'LAPTOP ACER NITRO 5 INTEL CORE I5 8GB RAM 512GB SSD 15.6\'\'', 3599, 'AN515-57-5323', 'ACER', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-acer-nitro-5-intel-core-i5-8gb-ram-512gb-ssd-156-2004292268368p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004292268368/2004292268368_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(9, 'LAPTOP APPLE MACBOOK AIR 8GB DDR4 256GB SSD 13\"', 4399, 'MacBook Air', 'APPLE', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-apple-macbook-air-8gb-ddr4-256gb-ssd-13-2004267031232p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004267031232/2004267031232_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(10, 'LAPTOP ASUS M1502IAEJ025W AMD RYZEN 5 4600H 8GB RAM 512GB SSD 15.6\'\'', 2099, 'M1502IA-EJ025W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-m1502iaej025w-amd-ryzen-5-4600h-8gb-ram-512gb-ssd-156-2004295829979p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004295829979/2004295829979_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(11, 'LAPTOP HP PAVILION GAMING AMD RYZEN 5 5600H 8GB RAM 512GB SSD 15.6\"', 3199, '15-ec2502la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-pavilion-gaming-amd-ryzen-5-5600h-8gb-ram-512gb-ssd-156-2004294667268p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004294667268/2004294667268_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(12, 'LAPTOP LENOVO IDEAPAD 5I INTEL CORE I5 16GB RAM 512GB SSD 15.6\"', 2699, 'IDEAPAD 5i', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-5i-intel-core-i5-16gb-ram-512gb-ssd-156-2004298827040p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004298827040/2004298827040_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(13, 'LAPTOP HP 14-DQ2514LA INTEL CORE I5 8GB RAM 256GB SSD 14\'\' + MOUSE Y ESTUCHE', 2299, '14-dq2514la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-14-dq2514la-intel-core-i5-8gb-ram-256gb-ssd-14-mouse-y-estuche-2004293401535p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004293401535/2004293401535_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(14, 'LAPTOP ASUS X515 X515EA-EJ921W INTEL CORE I5 1135G7 8GB RAM 512GB SSD 15.6\'\'', 2199, 'X515EA-EJ921W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-x515-x515ea-ej921w-intel-core-i5-1135g7-8gb-ram-512gb-ssd-156-2004296230507p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004296230507/2004296230507_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(15, 'LAPTOP LENOVO IDEAPAD 3I INTEL CORE I3 8GB RAM 256GB SSD 14\"', 1599, 'IDEAPAD 3i', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-3i-intel-core-i3-8gb-ram-256gb-ssd-14-2004298827033p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004298827033/2004298827033_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(16, 'LAPTOP ACER NITRO 5 INTEL CORE I5 8GB RAM 512GB SSD 15.6\'\'', 3299, 'AN515-57-57U5', 'ACER', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-acer-nitro-5-intel-core-i5-8gb-ram-512gb-ssd-156-2004292267064p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004292267064/2004292267064_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(17, 'LAPTOP LENOVO IDEAPAD 5I INTEL CORE I7 16GB RAM 512GB SSD 15.6\"', 3699, 'IDEAPAD 5i', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-5i-intel-core-i7-16gb-ram-512gb-ssd-156-2004298832174p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004298832174/2004298832174_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(18, 'LAPTOP LENOVO IDEAPAD GAMING 3 AMD RYZEN 5 8GB RAM 1TB HDD + 256GB SSD 15.6\'\'', 3899, 'IdeaPad Gaming 3', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-gaming-3-amd-ryzen-5-8gb-ram-1tb-hdd-256gb-ssd-156-2004295930378p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004295930378/2004295930378_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(19, 'LAPTOP ASUS TUF GAMING FA506QM-HN008W AMD RYZEN 7 16GB RAM 512GB SSD 15.6\"', 5149, 'FA506QM-HN008W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-tuf-gaming-fa506qm-hn008w-amd-ryzen-7-16gb-ram-512gb-ssd-156-2004298416633p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004298416633/2004298416633_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(20, 'LAPTOP HP 15-DY2503LA INTEL CORE I5 8GB RAM 512GB SSD 15.6\'\'', 2399, '15-dy2503la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-15-dy2503la-intel-core-i5-8gb-ram-512gb-ssd-156-2004293404673p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004293404673/2004293404673_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(21, 'LAPTOP ACER ASPIRE A3155851CG INTEL CORE I5 8GB RAM 512GB SSD 15.6\'\'', 2099, 'A315-58-51CG', 'ACER', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-acer-aspire-a3155851cg-intel-core-i5-8gb-ram-512gb-ssd-156-2004292239962p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004292239962/2004292239962_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(22, 'LAPTOP HP VICTUS 16-D0521LA INTEL CORE I7 16GB RAM 512GB SSD 16.1\'\'', 5099, 'Victus 16-d0521la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-victus-16-d0521la-intel-core-i7-16gb-ram-512gb-ssd-161-2004293401450p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004293401450/2004293401450_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(23, 'LAPTOP LENOVO IDEAPAD 3I INTEL CORE I5 8GB RAM 512GB SSD 15.6\"', 2099, 'IDEAPAD 3i', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-3i-intel-core-i5-8gb-ram-512gb-ssd-156-2004298827026p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004298827026/2004298827026_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(24, 'LAPTOP HP 15DY5010LA INTEL CORE I7 12GB RAM 512GB SSD 15.6\'\'', 3299, '15-dy5010la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-15dy5010la-intel-core-i7-12gb-ram-512gb-ssd-156-2004293401511p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004293401511/2004293401511_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(25, 'LAPTOP LENOVO IDEAPAD 5 AMD RYZEN 7 16GB RAM 512GB SSD 15.6\"', 3099, 'IDEAPAD 5', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-5-amd-ryzen-7-16gb-ram-512gb-ssd-156-2004298000016p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004298000016/2004298000016_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(26, 'LAPTOP MACBOOK AIR 13.3\" APPLE M1 256GB 8GB ORO ROSA', 4399, 'MacBook Air', 'APPLE', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-macbook-air-133-apple-m1-256gb-8gb-oro-rosa-2004267031263p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004267031263/2004267031263_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(27, 'LAPTOP ACER ASPIRE INTEL CORE I3 8GB RAM 256GB SSD 15.6\"', 1599, 'A315-56-34A1', 'ACER', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-acer-aspire-intel-core-i3-8gb-ram-256gb-ssd-156-2004297795180p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004297795180/2004297795180_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(28, 'LAPTOP ASUS VIVOBOOK 15 X1502ZA-BQ183W INTEL CORE I7 8GB RAM 512GB SSD 15.6\"', 4349, 'X1502ZA-BQ183W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-vivobook-15-x1502za-bq183w-intel-core-i7-8gb-ram-512gb-ssd-156-2004298416626p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004298416626/2004298416626_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(29, 'LAPTOP HP 250 G8 INTEL CORE I3 4GB RAM 1TB HDD 15.6\'\'', 1999, 'HP 250 G8 (2P5L7LT)', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-250-g8-intel-core-i3-4gb-ram-1tb-hdd-156-2004285094288p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004285094288/2004285094288_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(30, 'LAPTOP HP PAVILION 14-DV2007LA INTEL CORE I7 8GB RAM 512GB SSD 14\"', 3999, '14-dv2007la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-pavilion-14-dv2007la-intel-core-i7-8gb-ram-512gb-ssd-14-2004294661488p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004294661488/2004294661488_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(31, 'LAPTOP LENOVO IDEAPAD 5I INTEL CORE I7 8GB RAM 512GB SSD 14\'\'', 3399, 'IdeaPad 5', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-5i-intel-core-i7-8gb-ram-512gb-ssd-14-2004292142088p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004292142088/2004292142088_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(32, 'LAPTOP ASUS X515 X515JA-EJ2720W INTEL CORE I3 8GB RAM 256GB SSD 15.6\'\' FHD', 1499, 'X515JA-EJ2720W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-x515-x515ja-ej2720w-intel-core-i3-8gb-ram-256gb-ssd-156-fhd-2004288611284p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004288611284/2004288611284_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(33, 'LAPTOP ASUS ZENBOOK 13 OLED AMD RYZEN 5 8GB RAM 512GB SSD 13\'\'', 3199, 'UM325UA-KG168W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-zenbook-13-oled-amd-ryzen-5-8gb-ram-512gb-ssd-13-2004297878036p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004297878036/2004297878036_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(34, 'LAPTOP ASUS VIVOBOOK 15 OLED K513EA-L12004W INTEL CORE I5 1135G7 8GB RAM 512GB SSD 15.6\'\'', 3099, 'K513EA-L12004W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-vivobook-15-oled-k513ea-l12004w-intel-core-i5-1135g7-8gb-ram-512gb-ssd-156-2004296004740p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004296004740/2004296004740_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(35, 'LAPTOP ASUS VIVOBOOK PRO 15 OLED AMD RYZEN 5 16GB RAM 512GB SSD 15.6\"', 5599, 'M6500QC-L1070W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-vivobook-pro-15-oled-amd-ryzen-5-16gb-ram-512gb-ssd-156-2004298416664p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004298416664/2004298416664_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(36, 'LAPTOP ACER ASPIRE INTEL CORE I5 8GB RAM 512GB SSD 15.6\"', 2799, 'A315-59-51A0', 'ACER', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-acer-aspire-intel-core-i5-8gb-ram-512gb-ssd-156-2004297795357p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004297795357/2004297795357_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(37, 'LAPTOP ACER ASPIRE A315583405 INTEL CORE I3 8GB RAM 256GB SSD 15.6\'\'', 1599, 'A315-58-3405', 'ACER', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-acer-aspire-a315583405-intel-core-i3-8gb-ram-256gb-ssd-156-2004292271436p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004292271436/2004292271436_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(38, 'LAPTOP HP PAVILION GAMING AMD RYZEN 5 5600H 8GB RAM 512GB SSD 15.6\"', 3899, '15-fb0104la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-pavilion-gaming-amd-ryzen-5-5600h-8gb-ram-512gb-ssd-156-2004293401436p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004293401436/2004293401436_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(39, 'LAPTOP LENOVO IDEAPAD GAMING 3 AMD RYZEN 5 16GB RAM 512GB SSD 15.6\'\'', 4099, 'IdeaPad Gaming 3', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-gaming-3-amd-ryzen-5-16gb-ram-512gb-ssd-156-2004297243520p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004297243520/2004297243520_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(40, 'LAPTOP ACER A5155457MX INTEL CORE I5 8GB RAM 256GB SSD 15.6\'\'', 2099, 'A515-54-57MX', 'ACER', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-acer-a5155457mx-intel-core-i5-8gb-ram-256gb-ssd-156-2004295998828p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004295998828/2004295998828_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(41, 'LAPTOP LENOVO IDEAPAD GAMING 3I INTEL CORE I7 8GB RAM 512GB SSD 15.6\'\'', 4399, 'IdeaPad Gaming 3', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-gaming-3i-intel-core-i7-8gb-ram-512gb-ssd-156-2004288040947p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004288040947/2004288040947_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(42, 'LAPTOP ACER ASPIRE INTEL CORE I7 12GB RAM 512GB SSD 15.6\"', 3499, 'A315-59-72M1', 'ACER', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-acer-aspire-intel-core-i7-12gb-ram-512gb-ssd-156-2004297795197p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004297795197/2004297795197_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(43, 'LAPTOP HP VICTUS 16D0505LA INTEL CORE I5 11400H 16GB RAM 512GB SSD 16.1\'\'', 4899, 'Victus 16-d0505la ', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-victus-16d0505la-intel-core-i5-11400h-16gb-ram-512gb-ssd-161-2004284844242p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004284844242/2004284844242_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(44, 'LAPTOP LENOVO IDEAPAD 1I INTEL CELERON 4GB RAM 128GB SSD 14\'\'', 1399, 'IDEAPAD 1', 'LENOVO', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-lenovo-ideapad-1i-intel-celeron-4gb-ram-128gb-ssd-14-2004290369883p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004290369883/2004290369883_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(45, 'LAPTOP ASUS ZENBOOK UM425QA-KI172W AMD RYZEN 7 16GB RAM 512GB SSD 14\"', 4899, 'UM425QA-KI172W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-zenbook-um425qa-ki172w-amd-ryzen-7-16gb-ram-512gb-ssd-14-2004299240305p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004299240305/2004299240305_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(46, 'LAPTOP ASUS ZENBOOK DÃšO 14 UX482EGR-HY335W INTEL CORE I7 1195G7 16GB RAM 1TB SSD 14\"', 6399, 'UX482EGR-HY335W', 'ASUS', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-asus-zenbook-duo-14-ux482egr-hy335w-intel-core-i7-1195g7-16gb-ram-1tb-ssd-14-2004288608147p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004288608147/2004288608147_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(47, 'LAPTOP HP 14CF2513LA INTEL CORE I3 10110U 4GB RAM 128GB SSD 14\'\'', 1499, '14-cf2513la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-14cf2513la-intel-core-i3-10110u-4gb-ram-128gb-ssd-14-2004284771937p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004284771937/2004284771937_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png'),
(48, 'LAPTOP HP 15-EF2507LA AMD RYZEN 5 5500U 8GB RAM 512GB SSD 15.6\'\'', 2799, '15-ef2507la', 'HP', '001', 'RIPLEY', 'https://simple.ripley.com.pe/laptop-hp-15-ef2507la-amd-ryzen-5-5500u-8gb-ram-512gb-ssd-156-2004284813163p', 'https://home.ripley.com.pe/Attachment/WOP_5/2004284813163/2004284813163_2.jpg', 'https://1.bp.blogspot.com/-DkQozYMspOc/YZPjZeu2ZSI/AAAAAAAASC0/rl1K9CqHCoYMmk_OTWylXgSQH0j4F9Y8wCNcBGAsYHQ/s0/Ripley_logo_R_con_fondo_y_barras_2011-presente.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `utp_usuario`
--

DROP TABLE IF EXISTS `utp_usuario`;
CREATE TABLE `utp_usuario` (
  `CO_USUA` int(11) NOT NULL,
  `NO_USUA` varchar(255) NOT NULL,
  `NO_APEL` varchar(255) NOT NULL,
  `DE_CORR` varchar(255) NOT NULL,
  `DE_PASS` varchar(255) NOT NULL,
  `NO_TELF` int(9) NOT NULL,
  `DE_DIRE` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELACIONES PARA LA TABLA `utp_usuario`:
--

--
-- Volcado de datos para la tabla `utp_usuario`
--

INSERT INTO `utp_usuario` (`CO_USUA`, `NO_USUA`, `NO_APEL`, `DE_CORR`, `DE_PASS`, `NO_TELF`, `DE_DIRE`) VALUES
(1, 'Jennifer', 'Salas', 'Jennifer20108@gmail.com', '123456', 987654321, 'cambio'),
(2, 'Carlos', 'Rojas', 'carlos@gmail.com', 'Carlos', 975242331, 'Lurin'),
(3, 'Anita', 'Perez', 'Ana@gmail.com', '123456', 976060631, 'Comas');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `utp_categoria`
--
ALTER TABLE `utp_categoria`
  ADD PRIMARY KEY (`CO_CATE`);

--
-- Indices de la tabla `utp_lista_deseo`
--
ALTER TABLE `utp_lista_deseo`
  ADD PRIMARY KEY (`CO_LIST`),
  ADD KEY `FK_PROD` (`CO_PROD`),
  ADD KEY `FK_USUA` (`CO_USUA`),
  ADD KEY `CO_LIST` (`CO_LIST`);

--
-- Indices de la tabla `utp_producto`
--
ALTER TABLE `utp_producto`
  ADD PRIMARY KEY (`CO_PROD`),
  ADD KEY `FK_CATE` (`CO_CATE`);

--
-- Indices de la tabla `utp_usuario`
--
ALTER TABLE `utp_usuario`
  ADD KEY `CO_USUA` (`CO_USUA`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `utp_lista_deseo`
--
ALTER TABLE `utp_lista_deseo`
  MODIFY `CO_LIST` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
