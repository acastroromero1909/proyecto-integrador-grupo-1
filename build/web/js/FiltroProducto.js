function Filtrar() {

    var buscar = $("#buscar").val();
    $.ajax({
        type: "POST",
        data: {
            opc: "35",
            valor: buscar
        },
        dataType: 'json',
        url: "control",
        beforeSend: function (xhr) {
            $("#productos").html("Cargando...");
        },
        success: function (data, textStatus, xhr) {
            var result = "";
            if (data.length === 0) {
                result += "<div>No se encontraron coincidencias con el producto " + buscar + "</div>";
            } else {
                result += "<div class='row'>";
                for (var i in data) {
                    result += "<div class='col-sm-4'>";
                    result += "<div class='card text-center'>";
                    result += "<div class='card-body '>";
                    result += "<img src='ROPA/" + data[i].foto + "' width='200' height='180'>";
                    result += "</div>";
                    result += " <div class='card-footer'>";
                    result += " <label class='text-uppercase'>" + data[i].nompro + "</label>";
                    result += "<br>";
                    result += "<label>Precio : </label>S/. " + data[i].precio + "<br>";
                    result += "<a class='btn btn-outline-danger' href='control?opc=26&id=" + data[i].codpro + "'>Agregar al carrito</a>";
                    result += "</div>";
                    result += " </div>";
                    result += "</div>";
                }
                result += "</div>";
            }

            $("#productos").html(result);
        },
        error: function (xhr, status, errorThrown) {
            alert(errorThrown);
        }
    });
}