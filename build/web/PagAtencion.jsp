
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>GJ-Store</title>
    <link rel="icon" type="image" href="Images/logoSolo.png" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link href="css/inicio.css" rel="stylesheet" type="text/css"/>
        <script src="https://kit.fontawesome.com/d217ba9243.js" crossorigin="anonymous"></script>
        
        <link href="css/prueba.css" rel="stylesheet" type="text/css"/>
        
    </head>
    <header>
        <%@include file="recursos/Navbar.jsp" %>
    </header>
    <body style="background-color: #16191C;">
        
        <h2 style="color: #FFFFFF;">CONSULTAS</h2>
        <div class="card mb-3 w-75" id="cardP">
            <h2>NOSOTROS</h2>
            <BR>
          <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/bfxpublicos/salesupblog/100007999081.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Nuestra Historia</h5>
            <p class="card-text">Hola a todos, somos GJ-Store, una catalogo virtual que brindará miles de opciones de distintos productos tecnológicos.Nuestra historia comenzo con una simple pregunta como encontrar productos de una manera más rápida y económica y así surgimos nosotros, GJ-Store, con el principal objetivo de permitir ahorrar dinero a más peruanos en sus compras online al comparar los precios de miles de productos electrónicos de las tiendas más importantes del Perú.</p>
            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
          </div>
        </div>
          <h2 style="color: #FFFFFF;">NUESTRO EQUIPO</h2>
          <div class="profile-area">
              <div class="container">
                  <div class="row">
                      <div class="col-md-3">
                          <div class="card">
                              <div class="img1">
                                  <img src="https://www.nationalgeographic.com.es/medio/2018/02/27/playa-de-isuntza-lekeitio__1280x720.jpg">
                              </div>
                              <div class="img2">
                                  <img src="imagenes/equipo/Jennifer.jpeg" alt=""/>
                              </div>
                              <div class="main-text">
                                  <h2>Jennifer Salas</h2>
                                  <p>Programador Backend​ y Frontend<br>
                                      Ingeniería de Sistemas Décimo Ciclo</p>     
                              </div>
                              <div class="socials">
                                  <i class="fab fa-facebook"></i>
                                  <i class="fab fa-instagram"></i>
                                  <i class="fab fa-twitter"></i>
                                  <i class="fab fa-linkedin"></i>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="card">
                              <div class="img1">
                                  <img src="https://blog.foto24.com/wp-content/uploads/2021/08/Fotograf%C3%ADa_de_paisajes_Marinos_5.jpg">
                              </div>
                              <div class="img2">
                                  <img src="imagenes/equipo/Robin.jpg" alt=""/>
                              </div>
                              <div class="main-text">
                                  <h2>Robin Villegas</h2>
                                  <p>Programador Backend​ y Frontend<br>
                                      Ingeniería de Sistemas Noveno Ciclo</p>  

                              </div>
                              <div class="socials">
                                  <i class="fab fa-facebook"></i>
                                  <i class="fab fa-instagram"></i>
                                  <i class="fab fa-twitter"></i>
                                  <i class="fab fa-linkedin"></i>
                              </div>

                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="card">
                              <div class="img1">
                                  <img src="https://i.blogs.es/20f1ba/jumpstory-download20220511-143520/450_1000.jpg">
                              </div>
                              <div class="img2">
                                  <img src="imagenes/equipo/Alvaro.jpg" alt=""/>
                              </div>
                              <div class="main-text">
                                  <h2>Alvaro Castro</h2>
                                  <p>Programador Backend​ y Frontend<br>
                                      Ingeniería de Sistemas Noveno Ciclo</p> 

                              </div>
                              <div class="socials">
                                  <i class="fab fa-facebook"></i>
                                  <i class="fab fa-instagram"></i>
                                  <i class="fab fa-twitter"></i>
                                  <i class="fab fa-linkedin"></i>
                              </div>

                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="card">
                              <div class="img1">
                                  <img src="https://peru.info/archivos/publicacion/70-imagen-1414212822018.jpg">
                              </div>
                              <div class="img2">
                                  <img src="imagenes/equipo/Brandon.jpg" alt=""/>
                              </div>
                              <div class="main-text">
                                  <h2>Brandon Pantaleon</h2>
                                  <p>Programador Backend​ y Frontend<br>
                                      Ingeniería de Sistemas Noveno Ciclo</p> 

                              </div>
                              <div class="socials">
                                  <i class="fab fa-facebook"></i>
                                  <i class="fab fa-instagram"></i>
                                  <i class="fab fa-twitter"></i>
                                  <i class="fab fa-linkedin"></i>
                              </div>

                          </div>
                      </div>

                  </div>

              </div>

          </div>
          <h2 style="color: #FFFFFF;">CONSULTAS</h2>
      <div class="accordion w-75" id="accordionExample">
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
              <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <strong>¿Cómo agrego un producto a mi lista de favoritos?</strong>
              </button>
            </h2>
            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <p>Para agregar un producto a tu wishlist necesitas estar logueado en GJ-Store, luego solo necesitas dar click en el botón "Agregar a favoritos" en la página del producto. Adicionalmente, también puedes agregar un producto desde la galería dando click al ❤️‍.</p>
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingTwo">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <strong>¿El precio mostrado no coincide con el de la tienda?</strong>
              </button>
            </h2>
            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <p>Existe la pequeña posibilidad de que un precio no coincida. Esto se debe principalmente a que los precios dentro de compy no se encuentran en tiempo real y solo se actualizan 2 veces al día.</p>
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingThree">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  <strong>¿Cómo reporto si un producto está mal agrupado o presenta algún error?</strong>
              </button>
            </h2>
            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <p>            Para reportar un producto solo es necesario enviarnos un mensaje a nuestras redes con el link del producto y nosotros con gusto lo resolveremos.</p>
              </div>
            </div>
          </div>
           <div class="accordion-item">
            <h2 class="accordion-header" id="headingFour">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                  <strong>Políticas de seguridad</strong>
              </button>
            </h2>
            <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <p>GJ-Store adoptará las medidas necesarias y prudentes para resguardar la seguridad de tus datos y de tu clave secreta. En caso de detectarse cambios en la información que has registrado en el Sitio, o bien, ante cualquier irregularidad en las transacciones relacionadas con tu identificación o la del medio de pago, o simplemente como medida de protección a tu identidad, nuestros ejecutivos podrán contactarte por vía telefónica o correo electrónico, a fin de corroborar tus datos e intentar evitar posibles fraudes.</p>
              </div>
            </div>
          </div>
        </div>
    </body>
</html>
