<%@page import="modeloDAO.productoDAO"%>
<%@page import="modelo.*, java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link href="css/inicio.css" rel="stylesheet" type="text/css"/>
        <script src="https://kit.fontawesome.com/d217ba9243.js" crossorigin="anonymous"></script>
        <%@include file="recursos/Navbar.jsp" %>
        <title>JSP Page</title>
    </head>
    <body id="cuerpo" >
        <%
           
            HttpSession sesion = request.getSession();
            String correo = (String) sesion.getAttribute("correo");
            String nombre = (String) sesion.getAttribute("nom");
            //List<Producto> productos = (ArrayList) sesion.getAttribute("productos");
            List<ListaDeseos> listaD = (ArrayList) sesion.getAttribute("listaD");

        %>
        <div class="content" style="display: flex; justify-content: center;">
        <div class="card" style="width: 95%; ">
            <div class="card-body" style="display: flex; justify-content: center;">
        <table  width="95%"  class="table">
            <thead>
            <tr>
                <th scope="col">IMAGEN</th>
                <th scope="col">PRODUCTO</th>
                <th scope="col">PRECIO</th>
                <th scope="col">MEJOR TIENDA</th>
                <th scope="col">OPCIONES</th>

            </tr>
            </thead>
            <c:forEach  var="producto" items="${listaD}">
                <tr>
                    <td><a href="control?opc=26&id=${producto.getProducto().id}"><img src="${producto.getProducto().linkProIMG}" width="100" height="100"></td></a>
                    
                    <td><a href="control?opc=26&id=${producto.getProducto().id}">${producto.getProducto().nombre}</td></a>
                    <td>S/.${producto.getProducto().precio}0</td><!-- comment -->
                    <td><img src="${producto.getProducto().tiendaIMG}" width="100" height="100"></td>
                    <td> <a class="btn btn-outline-danger" href="control?opc=eliminar&idCodLista=${producto.codLista}"><i class="fa-solid fa-trash-can"></i>  Eliminar</a></td>
                </tr>

            </c:forEach>
            
        </table>
            </div>
            </div>
            </div>
    </body>
</html>
