<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="modeloDAO.productoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="modelo.*, java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>GJ-Store</title>
        <link rel="icon" type="image" href="Images/logoSolo.png" >
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link href="css/inicio.css" rel="stylesheet" type="text/css"/>
        <script src="https://kit.fontawesome.com/d217ba9243.js" crossorigin="anonymous"></script>
        <%@include file="recursos/Navbar.jsp" %>
        
    </head>
    <body id="cuerpo" style="background-color: #16191C;">

        <%

            List<Producto> lis = (ArrayList) request.getAttribute("dato");
            Producto p = new Producto();
            HttpSession sesion = request.getSession();
            String correo = (String) sesion.getAttribute("correo");
            String nombre = (String) sesion.getAttribute("nom");
            Integer idU = (Integer) sesion.getAttribute("id");
        %>


        <% if ((correo==null && nombre==null && idU==null) || (correo!=null && nombre!=null && idU!=null)) {%>

    <center>
        <h1 id="subti"><%=lis.get(0).getNomCategoria()%></h1>
        <!--<select name="select">
        <option value="value1">Value 1</option>
        <option value="value2" selected>Value 2</option>
        <option value="value3">Value 3</option>-->
    </select>
    <div class="container mt-4" >
        <div class="row">
            <%
                for (Producto x : lis) {
            %>

            <div class="col-sm-4">
                <a href="control?opc=26&id=<%=x.getId()%>">  
                    <div class="card-deck text-center">

                        <div class="card-body ">

                            <img src="<%=x.getLinkProIMG()%>" width="200" height="180">
                        </div>
                        <div class="card-footer">
                            <label class="text-uppercase"><%=x.getNombre()%></label><br>
                            <label>Precio : </label>S/. <%=x.getPrecio()%>0<br>
                           

                        </div>
                    </div>

            </div>
            </a>
            <%
                }
            %>

        </div>
        <br><br>
    </div>



</center>

<% }%>



</body>
</html>

