/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.Usuario;
import util.MySQLConexion;

/**
 *
 * @author user
 */
public class usuarioDAO {
        Connection conn = null;

    public List<Usuario> LisUsuario() {
        List<Usuario> lis = new ArrayList<>();
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM utp_usuario ";
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Usuario u = new Usuario();
                u.setIdUser(rs.getInt(1));
                u.setNomUser(rs.getString(2));
                u.setApeUser(rs.getString(3));
                u.setCorrUser(rs.getString(4));
                u.setPassUser(rs.getString(5));
                u.setCelUser(rs.getInt(6));
                u.setDirUser(rs.getString(7));
                lis.add(u);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
        return lis;

    }

    public void del_usuario(int id) {
        String codigo = "";
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            //invocando a un procedure
            String sql = "Delete from utp_usuario where CO_USUA =?";

            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }

    }

    public String Adi_usuario(Usuario u) {
        String codigo = "";
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            //invocando a un procedure
            String sql = "{call spAdiUser(?,?,?,?,?,?)}";
            //PreparedStatement st2=conn.prepareStatement("insert into alumno(?,?,?,?,?)");
            CallableStatement st = conn.prepareCall(sql);

            st.setString(1, u.getNomUser());
            st.setString(2, u.getApeUser());
            st.setString(3, u.getCorrUser());
            st.setString(4, u.getPassUser());
            st.setInt(5, u.getCelUser());
            st.setString(6, u.getDirUser());
            ResultSet rs = st.executeQuery();
            //llenar el arraylist con la clase entidad
            if (rs.next()) {
                codigo = rs.getString(1);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }

        return codigo;
    }

    public Usuario BusUsuario(int id) {
        Usuario u = null;
        
        try {
            conn = MySQLConexion.getConexion();
            String sql = "select `CO_USUA`, `NO_USUA`, `NO_APEL`, `DE_CORR`, `DE_PASS`, `NO_TELF`, `DE_DIRE` from utp_usuario where CO_USUA=?;";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            //llenar el arraylist con la clase entidad
            if (rs.next()) {
                u = new Usuario();
                u.setIdUser(rs.getInt(1));
    
                u.setNomUser(rs.getString(2));
                u.setApeUser(rs.getString(3));
                u.setCorrUser(rs.getString(4));
                u.setPassUser(rs.getString(5));
                u.setCelUser(rs.getInt(6));
                u.setDirUser(rs.getString(7));

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }

        return u;
    }

    public void Modi_usuario(Usuario u) {
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "{call cambiaUser(?,?,?,?,?,?,?)}";
            CallableStatement st = conn.prepareCall(sql);
            st.setInt(1, u.getIdUser());
            st.setString(2, u.getNomUser());
            st.setString(3, u.getApeUser());
            st.setString(4, u.getCorrUser());
            st.setString(5, u.getPassUser());
            st.setInt(6, u.getCelUser());
            st.setString(7, u.getDirUser());
            st.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
    }

    public int Validar(Usuario u) {

        Connection conn = null;
        int r = 0;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "Select DE_CORR , DE_PASS,CO_USUA ,NO_USUA from utp_usuario where DE_CORR=? and DE_PASS=? ";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, u.getCorrUser());
            st.setString(2, u.getPassUser());

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                r = r + 1;
                u.setCorrUser(rs.getString(1));
                u.setPassUser(rs.getString(2));
                u.setIdUser(rs.getInt(3));
                u.setNomUser(rs.getString(4));

            }
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
        return 0;
    }

}
