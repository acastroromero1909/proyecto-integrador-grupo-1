package modeloDAO;

import Interfaces.tiendaInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Categoria;
import modelo.Producto;
import modelo.Usuario;
import util.MySQLConexion;

public class productoDAO implements tiendaInter {

    Connection conn = null;

    @Override
    public List<Producto> LisProCat(String id) {
        List<Producto> lis = new ArrayList<>();

        try {
            conn = MySQLConexion.getConexion();
            String sql = "SELECT CO_PROD, DE_PROD, MIN(IM_PREC), DE_MODE, DE_MARC, utp_producto.CO_CATE, DE_TIEN,LK_TIEN, LK_IMAG, LK_LOGOTIEN, utp_categoria.DE_CATE FROM utp_producto INNER JOIN utp_categoria on utp_producto.CO_CATE=utp_categoria.CO_CATE WHERE utp_producto.CO_CATE=? GROUP BY DE_MODE;";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            //llenar el arraylist con la clase entidad
            while (rs.next()) {
                Producto p = new Producto();
                p.setId(rs.getInt(1));
                p.setNombre(rs.getString(2));
                p.setPrecio(rs.getDouble(3));
                p.setModelo(rs.getString(4));
                p.setMarca(rs.getString(5));
                p.setCategoria(rs.getString(6));
                p.setTiendaNom(rs.getString(7));
                p.setLinkTienda(rs.getString(8));
                p.setLinkProIMG(rs.getString(9));
                p.setTiendaIMG(rs.getString(10));
                p.setNomCategoria(rs.getString(11));

                lis.add(p);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }

        return lis;
    }

    public List<Producto> LisPro(int id) {
        List<Producto> lis = new ArrayList<>();

        try {
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM `utp_producto` WHERE `DE_MODE` =(SELECT `DE_MODE` FROM utp_producto WHERE `CO_PROD`=?) order by `IM_PREC`;";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            //llenar el arraylist con la clase entidad
            while (rs.next()) {
                Producto p = new Producto();
                p.setId(rs.getInt(1));
                p.setNombre(rs.getString(2));
                p.setPrecio(rs.getDouble(3));
                p.setModelo(rs.getString(4));
                p.setMarca(rs.getString(5));
                p.setCategoria(rs.getString(6));
                p.setTiendaNom(rs.getString(7));
                p.setLinkTienda(rs.getString(8));
                p.setLinkProIMG(rs.getString(9));
                p.setTiendaIMG(rs.getString(10));
                lis.add(p);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }

        return lis;
    }

    public List<Categoria> LisCategoria() {
        List<Categoria> lis = new ArrayList<>();

        try {
            conn = MySQLConexion.getConexion();
            String sql = "select idCargo,nomCargo from cargo";
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Categoria c = new Categoria();
                c.setCodcate(rs.getString(1));
                c.setNomcate(rs.getString(2));
                lis.add(c);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
        return lis;

    }

    public Producto encontrarProducto(Producto producto) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String SQL_SELECT_BY_ID = "SELECT CO_PROD,DE_PROD,DE_TIEN,LK_TIEN,IM_PREC,LK_IMAG,LK_LOGOTIEN FROM utp_producto WHERE CO_PROD=?";
        try {
            conn = MySQLConexion.getConexion();
            stmt = conn.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setInt(1, producto.getId());
            rs = stmt.executeQuery();
            rs.next();//nos posicionamos en el primer registro devuelto

            int idProducto = rs.getInt("CO_PROD");
            String descripcion = rs.getString("DE_PROD");
            String nomTienda = rs.getString("DE_TIEN");
            String urlTienda = rs.getString("LK_TIEN");
            double precio = rs.getDouble("IM_PREC");
            String imgProd = rs.getString("LK_IMAG");
            String imgTienda = rs.getString("LK_LOGOTIEN");
            producto.setId(idProducto);
            producto.setTiendaNom(nomTienda);
            producto.setLinkTienda(urlTienda);
            producto.setPrecio(precio);
            producto.setLinkProIMG(imgProd);
            producto.setTiendaIMG(imgTienda);

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return producto;
    }
    
    
    
}
