package modeloDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.ListaDeseos;
import modelo.Producto;
import modelo.Usuario;
import util.MySQLConexion;

public class ListaDeseosDAO {

    public int insertDeseo(ListaDeseos listaDeseos) {
        String SQL = "Insert into utp_lista_deseo(CO_PROD,CO_USUA,FE_REGI) values (?,?,?) ";
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        try {
            conn = MySQLConexion.getConexion();
            stmt = conn.prepareStatement(SQL);
            stmt.setInt(1, listaDeseos.getProducto().getId());
            stmt.setInt(2, listaDeseos.getUsuario().getIdUser());
            stmt.setString(3, listaDeseos.getFecha());
            rows = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

        return rows;
    }

    public List<Producto> listaProductos(int codUsuario) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Producto> lista = new ArrayList<>();
        String SQL_SELECT_BY_ID = "SELECT P.LK_IMAG,P.DE_PROD,P.IM_PREC,P.LK_LOGOTIEN,P.DE_TIEN"
                + " FROM  utp_lista_deseo AS D INNER JOIN utp_producto AS P ON"
                + " D.CO_PROD=P.CO_PROD WHERE D.CO_USUA=?";
        try {
            conn = MySQLConexion.getConexion();
            stmt = conn.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setInt(1, codUsuario);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setLinkProIMG(rs.getString(1));
                producto.setNombre(rs.getString(2));
                producto.setPrecio(rs.getDouble(3));
                producto.setTiendaIMG(rs.getString(4));
                producto.setTiendaNom(rs.getString(5));
                lista.add(producto);
            }


        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return lista;
    }
    public List<ListaDeseos> listaProductosD(int codUsuario) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<ListaDeseos> lista = new ArrayList<>();
        String SQL_SELECT_BY_ID = "SELECT D.CO_LIST,P.DE_PROD,P.IM_PREC,P.DE_TIEN,P.LK_IMAG,P.LK_LOGOTIEN, P.CO_PROD"
                + " FROM  utp_lista_deseo AS D INNER JOIN utp_producto AS P ON"
                + " D.CO_PROD=P.CO_PROD WHERE D.CO_USUA=?";
        try {
            conn = MySQLConexion.getConexion();
            stmt = conn.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setInt(1, codUsuario);
            rs = stmt.executeQuery();
            while (rs.next()) {
                ListaDeseos listaDeseos = new ListaDeseos();
                listaDeseos.setCodLista(rs.getInt(1));
                listaDeseos.setProducto(new Producto(rs.getString(2),rs.getDouble(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getInt(7)));
                lista.add(listaDeseos);
            }


        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return lista;
    }
    
    public int delete(ListaDeseos listaDeseos) {
       Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        String SQL_DELETE="DELETE  FROM utp_lista_deseo WHERE CO_LIST = ?";
        try {
            conn = MySQLConexion.getConexion();
            stmt = conn.prepareStatement(SQL_DELETE);
            stmt.setInt(1, listaDeseos.getCodLista());
            rows = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } 
        return rows;
    }
    
    public boolean encontrarProducto2(Producto producto, Usuario usuario) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String SQL_SELECT_BY_ID = "SELECT CO_PROD,CO_USUA FROM utp_lista_deseo WHERE (CO_PROD=? AND CO_USUA=?)";
 
        boolean res= false;
        try {
            conn = MySQLConexion.getConexion();
            stmt = conn.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setInt(1, producto.getId());
            stmt.setInt(2, usuario.getIdUser());
            rs = stmt.executeQuery();
            res = rs.next();//nos posicionamos en el primer registro devuelto

            if(res){
                System.out.println("Encontrado");
            }else{
                System.out.println("No se encontro el producto");
            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return res;
    }
}
