/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.ListaDeseos;
import modelo.Producto;
import modelo.Usuario;
import modeloDAO.ListaDeseosDAO;
import modeloDAO.productoDAO;
import modeloDAO.usuarioDAO;

@WebServlet("/ServletControlador")
public class control extends HttpServlet {

    productoDAO objP = new productoDAO();
    usuarioDAO objU = new usuarioDAO();
    ListaDeseosDAO objD = new ListaDeseosDAO();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String op = request.getParameter("opc");
        if (op.equals("0")) {
            validar(request, response);
        }

        if (op.equals("7")) {
            adicionUsuario(request, response);
        }
        if (op.equals("8")) {
            EditUsuario(request, response);
        }
        if (op.equals("10")) {
            CambiaUsuario(request, response);
        }
        if (op.equals("13")) {
            listaPro(request, response);
        }
        if (op.equals("agregarDeseo")) {
            this.agregarCarrito(request, response);
        }
        if (op.equals("29")) {
            cerrarSesion(request, response);
        }
        if (op.equals("26")) {
            this.listaProBAMBA(request, response);
        }
        if (op.equals("eliminar")) {
            this.eliminarDeseo(request, response);
        }

    }

    public void ListarDeseo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<ListaDeseos> listaD = objD.listaProductosD(idU);
        request.getSession().setAttribute("listaD", listaD);
        request.getRequestDispatcher("/pagDeseos.jsp").forward(request, response);

    }

    public void eliminarDeseo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idcodList = Integer.parseInt(request.getParameter("idCodLista"));
        int res = objD.delete(new ListaDeseos(idcodList));
        String resultado = (res == 1) ? "Eliminado" : "No se elimino";
        System.out.println("El valor es:" + resultado);
        this.ListarDeseo(request, response);

    }
    int idU;

    public void agregarCarrito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int idtProd = Integer.parseInt(request.getParameter("id"));
        idU = Integer.parseInt(request.getParameter("idU"));

        boolean encontrar = objD.encontrarProducto2(new Producto(idtProd), new Usuario(idU));
        if (encontrar) {
            System.out.println("Producto Encontrado dentro del Usuario:" + idU);
        } else {
            System.out.println("El valor del id enviado es:" + idtProd);
            //producto
            Producto producto = objP.encontrarProducto(new Producto(idtProd));
            String resultado = (producto == null) ? "No encontrado" : "Encontrado";
            System.out.println("Producto:" + resultado);

            //usuario
            Usuario usuario = objU.BusUsuario(idU);
            //obtener fecha
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String fecha = dateFormat.format(new Date());

            ListaDeseos listaDeseos = new ListaDeseos(producto, usuario, fecha);
            int valor = objD.insertDeseo(listaDeseos);
            if (valor == 1) {
                System.out.println("Agregado a lista");
            } else {
                System.out.println("No agregado");
            }
        }

        //lista productos ListaDeseos en base al idUsuario
        //List<Producto> productos = objD.listaProductos(idU);
        List<ListaDeseos> listaD = objD.listaProductosD(idU);

        //request.getSession().setAttribute("productos", productos);
        request.getSession().setAttribute("listaD", listaD);
        request.setAttribute("idProd", idtProd);
        request.getRequestDispatcher("/pagDeseos.jsp").forward(request, response);

    }
int idG;
    protected void validar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Usuario u = new Usuario();
        u.setCorrUser(request.getParameter("correo"));
        u.setPassUser(request.getParameter("contrasena"));
        System.out.println("El valor Encontrado del Producto Seleccionado : " +id);
        int r = objU.Validar(u);
        if (r == 1) {
            request.getSession().setAttribute("correo", request.getParameter("correo"));
            request.getSession().setAttribute("id", u.getIdUser());
            request.getSession().setAttribute("nom", u.getNomUser());

            String pag;
            pag = "/index.jsp";
            request.setAttribute("dato", "Gracias por iniciar sesion");
            if(id==0){
                request.getRequestDispatcher(pag).forward(request, response);
            }else{
                request.getRequestDispatcher("ServletControlador?opc=26&id="+id).forward(request, response);
            }
        } else {
            request.getRequestDispatcher("/Login.jsp").forward(request, response);
        }

    }

    protected void listaPro(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cod = request.getParameter("id");
        System.out.println("El idrecuperado es:" + cod);
        request.setAttribute("dato", objP.LisProCat(cod));
        String pagina = "/PagPro.jsp";
        request.getRequestDispatcher(pagina).forward(request, response);
    }
int id;
    protected void listaProBAMBA(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         id = Integer.parseInt(request.getParameter("id"));
        System.out.println("El valor Encontrado del Producto Seleccionado : " +id);
        request.setAttribute("dato", objP.LisPro(id));
        String pagina = "/pagDetalles.jsp";
        request.getRequestDispatcher(pagina).forward(request, response);
    }

    protected void cerrarSesion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        session.invalidate();
        id = 0;
        request.getRequestDispatcher("/index.jsp").forward(request, response);

    }

    protected void adicionUsuario(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Usuario u = new Usuario();
        u.setNomUser(request.getParameter("nombre"));
        u.setApeUser(request.getParameter("apellido"));
        u.setCorrUser(request.getParameter("correo"));
        u.setPassUser(request.getParameter("contrasena"));
        u.setCelUser(Integer.parseInt(request.getParameter("celular")));
        u.setDirUser(request.getParameter("direccion"));
        String cod = objU.Adi_usuario(u);
        request.getRequestDispatcher("/Login.jsp").forward(request, response);
    }

    protected void EditUsuario(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int cod = (Integer.parseInt(request.getParameter("id"))); //recibe el codigo de especialidad
        request.setAttribute("dato", objU.BusUsuario(cod));
        request.getRequestDispatcher("/regUsuarioEdit.jsp").forward(request, response);
    }

    protected void CambiaUsuario(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Usuario u = new Usuario();
        u.setIdUser(Integer.parseInt(request.getParameter("codu")));
        u.setNomUser(request.getParameter("nombre"));
        u.setApeUser(request.getParameter("apellido"));
        u.setCorrUser(request.getParameter("correo"));
        u.setPassUser(request.getParameter("contrasena"));
        u.setCelUser(Integer.parseInt(request.getParameter("celular")));
        u.setDirUser(request.getParameter("direccion"));
        objU.Modi_usuario(u);
        validar(request, response);
    }

    protected void listaCategoria(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cod = request.getParameter("id");
        request.setAttribute("dato", objP.LisCategoria());
        String pagina = "";
        request.getRequestDispatcher(pagina).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
