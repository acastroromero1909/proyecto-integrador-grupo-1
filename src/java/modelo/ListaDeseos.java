/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author Brandon
 */
public class ListaDeseos {
    private int codLista;
    private Producto producto;
    private Usuario usuario;
    private String fecha;
    public ListaDeseos() {
    }
    
    public ListaDeseos(Producto producto, Usuario usuario, String fecha) {
        this.producto = producto;
        this.usuario = usuario;
        this.fecha = fecha;
    }

    public ListaDeseos(int codLista, Producto producto, Usuario usuario, String fecha) {
        this.codLista = codLista;
        this.producto = producto;
        this.usuario = usuario;
        this.fecha = fecha;
    }

    public ListaDeseos(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public ListaDeseos(int codLista) {
        this.codLista = codLista;
    }

    public int getCodLista() {
        return codLista;
    }

    public void setCodLista(int codLista) {
        this.codLista = codLista;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
    
}
