
package modelo;

public class Producto {
    int id;
    String nombre;
    double precio;
    String modelo;
    String marca;
    String categoria;
    String tiendaNom;
    String linkTienda;
    String linkProIMG;
    String tiendaIMG;
    String nomCategoria;
    
    public Producto() {
    }

    public Producto(int id) {
        this.id = id;
    }

    public Producto(String nombre, double precio, String tiendaNom, String linkProIMG, String tiendaIMG,int id) {
        this.nombre = nombre;
        this.precio = precio;
        this.tiendaNom = tiendaNom;
        this.linkProIMG = linkProIMG;
        this.tiendaIMG = tiendaIMG;
       this.id = id;
    }
    
    public Producto(int id, String nombre, double precio, String modelo, String marca, String categoria, String tiendaNom, String linkTienda, String linkProIMG, String tiendaIMG, String nomCategoria) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.modelo = modelo;
        this.marca = marca;
        this.categoria = categoria;
        this.tiendaNom = tiendaNom;
        this.linkTienda = linkTienda;
        this.linkProIMG = linkProIMG;
        this.tiendaIMG = tiendaIMG;
        this.nomCategoria = nomCategoria;
    }

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getTiendaNom() {
        return tiendaNom;
    }

    public void setTiendaNom(String tiendaNom) {
        this.tiendaNom = tiendaNom;
    }

    public String getLinkTienda() {
        return linkTienda;
    }

    public void setLinkTienda(String linkTienda) {
        this.linkTienda = linkTienda;
    }

    public String getLinkProIMG() {
        return linkProIMG;
    }

    public void setLinkProIMG(String linkProIMG) {
        this.linkProIMG = linkProIMG;
    }

    public String getTiendaIMG() {
        return tiendaIMG;
    }

    public void setTiendaIMG(String tiendaIMG) {
        this.tiendaIMG = tiendaIMG;
    }

    public String getNomCategoria() {
        return nomCategoria;
    }

    public void setNomCategoria(String nomCategoria) {
        this.nomCategoria = nomCategoria;
    }

    
    
    
    
 
}
