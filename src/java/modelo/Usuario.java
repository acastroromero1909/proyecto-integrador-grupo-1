/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author user
 */
public class Usuario {
    private int idUser;
    private String nomUser;
    private String apeUser;
    private String corrUser;
    private String passUser;
    private int celUser;
    private String dirUser;

    public Usuario() {
    }

    public Usuario(int idUser) {
        this.idUser = idUser;
    }

    
    public Usuario(int idUser, String nomUser, String apeUser, String corrUser, String passUser, int celUser, String dirUser) {
        this.idUser = idUser;
        this.nomUser = nomUser;
        this.apeUser = apeUser;
        this.corrUser = corrUser;
        this.passUser = passUser;
        this.celUser = celUser;
        this.dirUser = dirUser;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    

    public String getNomUser() {
        return nomUser;
    }

    public void setNomUser(String nomUser) {
        this.nomUser = nomUser;
    }

    public String getApeUser() {
        return apeUser;
    }

    public void setApeUser(String apeUser) {
        this.apeUser = apeUser;
    }

    public String getCorrUser() {
        return corrUser;
    }

    public void setCorrUser(String corrUser) {
        this.corrUser = corrUser;
    }

    public String getPassUser() {
        return passUser;
    }

    public void setPassUser(String passUser) {
        this.passUser = passUser;
    }

    public int getCelUser() {
        return celUser;
    }

    public void setCelUser(int celUser) {
        this.celUser = celUser;
    }

    public String getDirUser() {
        return dirUser;
    }

    public void setDirUser(String dirUser) {
        this.dirUser = dirUser;
    }
    
    
     

}
