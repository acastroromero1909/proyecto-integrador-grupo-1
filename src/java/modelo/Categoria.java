
package modelo;


public class Categoria {
    
    String codcate;
    String nomcate;

    public String getCodcate() {
        return codcate;
    }

    public void setCodcate(String codcate) {
        this.codcate = codcate;
    }

    public String getNomcate() {
        return nomcate;
    }

    public void setNomcate(String nomcate) {
        this.nomcate = nomcate;
    }
    
    
    
}
